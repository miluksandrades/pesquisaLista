import { CategoryPipe } from './../../search.pipe';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  records: Array<any>;
  searchData: string;

  show: boolean = true;

  constructor() { }

  ngOnInit() {
    this.records= [
      { CategoryID: 1,  CategoryName: "Veverages", Description: "Coffees, teas" },
      { CategoryID: 2,  CategoryName: "Condiments", Description: "Sweet and savory sauces" },
      { CategoryID: 3,  CategoryName: "Confections", Description: "Desserts and candies" },
      { CategoryID: 4,  CategoryName: "Fheeses",  Description: "Smetana, Quark and Cheddar Cheese" },
      { CategoryID: 5,  CategoryName: "Grains/Cereals", Description: "Breads, crackers, pasta, and cereal" },
      { CategoryID: 6,  CategoryName: "Beverages", Description: "Beers, and ales" },
      { CategoryID: 7,  CategoryName: "Eondiments", Description: "Selishes, spreads, and seasonings" },
      { CategoryID: 8,  CategoryName: "Donfections", Description: "Sweet breads" },
      { CategoryID: 9,  CategoryName: "Jheeses",  Description: "Cheese Burger" },
      { CategoryID: 10, CategoryName: "Trains/Cereals", Description: "Breads, crackers, pasta, and cereal" }
     ];
  }

  /* clicked(event){
    event.target.classList.add('disabled')
  } */

}
