import { Pipe, PipeTransform } from "../../node_modules/@angular/core";

@Pipe({name: 'category'})
export class CategoryPipe implements PipeTransform{
    transform(categories: any, seachText: any): any{
        if(seachText == null) return categories;

        return categories.filter((category) => {
            return category.CategoryName.toLowerCase().indexOf(seachText.toLowerCase()) > -1;
        })
    }
}