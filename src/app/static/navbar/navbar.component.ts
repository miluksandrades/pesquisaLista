import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  menuItem: Array<any>;

  constructor() { }

  ngOnInit() {
    this.menuItem = [
      {"Icon": "home", "Title": "Início", "router": ''},
      {"Icon": "description", "Title": "description", "router": "description"},
      {"Icon": "android", "Title": "android", "router": "android"},
      {"Icon": "alarm", "Title": "alarm", "router": "alarm"},
    ]
  }

}
